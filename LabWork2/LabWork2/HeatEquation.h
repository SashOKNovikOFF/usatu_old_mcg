// /////////////////////////////////////////////////////////////////////////////////////
// ���������� ��������� ���������������� (����� �����)
// �.�. �������
// /////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>
#include <fstream>
#include <iomanip>
#include <string>

#include "TDM_Algorithm.h"

class HeatEquation;

class HeatEquation {

protected:

	const double length = 0.2;

	const double coeff_ro = 7800.0;
	const double coeff_c = 460.0;
	const double coeff_lambda = 46.0;

	const double initial = 50.0;

	const double coeff_T0 = 300.0;
	const double coeff_q = 1.0E5;

	const double coeff_a = coeff_lambda / coeff_c / coeff_ro;

	int numNode;
	double dx;
	double dt;

	double time;

	std::vector<double> oldData;
	std::vector<double> newData;

public:

	HeatEquation(int numNode_, double dt_) :
		numNode(numNode_), dx(length / (numNode_ - 1)), dt(dt_), oldData(numNode_, initial), newData(numNode_, 0.0), time(0.0)
	{
		std::cout << std::endl << "Kurant - " + std::to_string(dx * dx / 4.0 / coeff_a);
		if (dt_ > dx * dx / 4.0 / coeff_a)
		{
			std::string error;
			std::cout << std::endl << "Error: Kurant - " + std::to_string(dx * dx / 4.0 / coeff_a) + ", dt - " + std::to_string(dt_) << std::endl;
		};
		oldData[0] = coeff_T0;
		oldData[numNode - 1] = initial + coeff_q * dx / coeff_lambda;
	};
	~HeatEquation() { };

	void calcOneStepEXPL() {

		newData[0] = coeff_T0;
		for (int i = 1; i < numNode - 1; i++)
			newData[i] = oldData[i] + dt * coeff_a * (oldData[i - 1] - 2 * oldData[i] + oldData[i + 1]) / dx / dx;
		newData[numNode - 1] = newData[numNode - 2] + coeff_q * dx / coeff_lambda;

		oldData = newData;
		time += dt;
	};
	void calcOneStepTDMA()
	{
		TDM_Algorithm solver(numNode);

		solver.setLeftBound(1.0, 0.0, coeff_T0);
		solver.setRightBound(1.0, -1.0, -coeff_q * dx / coeff_lambda);

		for (int node = 1; node < numNode - 1; node++)
			solver.setValues(node, 1.0, -dx * dx / coeff_a / dt - 2.0, 1.0, - oldData[node] * dx * dx / coeff_a / dt);

		solver.solve();
		newData = solver.getSolution();

		oldData = newData;
		time += dt;
	};

	bool getCheckResult() { return (dt <= dx * dx / 2 / coeff_a) ? true : false; };
	std::vector<double> getSolution() { return newData; };
	double getTime() { return time; };
	double getDt() { return dt; };
	double getDx() { return dx; };

	void printNumericalSolution(const std::string outputFile) {

		std::ofstream myFile(outputFile);

		myFile.precision(12);
		myFile << std::scientific;

		myFile << std::setw(12) << "X" << " ";
		myFile << std::setw(12) << "W" << std::endl;

		for (int node = 0; node < numNode; node++)
			myFile << node * dx << " " << newData[node] << std::endl;
		myFile.close();
	};
};