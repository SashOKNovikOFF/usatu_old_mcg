#pragma once

#include <vector>

class TDM_Algorithm;

class TDM_Algorithm
{
protected:

	int size;
	std::vector< std::vector<double> > matrix;
	std::vector<double> alpha;
	std::vector<double> beta;
	std::vector<double> solution;

public:

	TDM_Algorithm(int size_) : size(size_),
		matrix(4, std::vector<double>(size_, 0.0)),
		alpha(size_ - 1, 0.0),
		beta(size_, 0.0),
		solution(size_, 0.0) { };

	void setLeftBound(double c0, double b0, double f0)
	{
		matrix[0][0] = 0.0;
		matrix[1][0] = -b0;
		matrix[2][0] = c0;
		matrix[3][0] = f0;
	};
	void setRightBound(double aN, double cN, double fN)
	{
		matrix[0][size - 1] = -aN;
		matrix[1][size - 1] = 0.0;
		matrix[2][size - 1] = cN;
		matrix[3][size - 1] = fN;
	};
	void setValues(int index, double ai, double ci, double bi, double fi)
	{
		matrix[0][index] = -ai;
		matrix[1][index] = -bi;
		matrix[2][index] = ci;
		matrix[3][index] = fi;
	};
	void solve()
	{
		// ������ ��� ��������

		alpha[0] = matrix[1][0] / matrix[2][0];
		beta[0] = matrix[3][0] / matrix[2][0];
		for (std::size_t i = 1; i < alpha.size(); i++)
		{
			alpha[i] = matrix[1][i] / (matrix[2][i] - matrix[0][i] * alpha[i - 1]);
			beta[i] = (matrix[3][i] + matrix[0][i] * beta[i - 1]) / (matrix[2][i] - matrix[0][i] * alpha[i - 1]);
		};
		beta[size - 1] = (matrix[3][size - 1] + matrix[0][size - 1] * beta[size - 2]) / (matrix[2][size - 1] - matrix[0][size - 1] * alpha[size - 2]);

		// �������� ��� ��������

		solution[size - 1] = beta[size - 1];
		for (int i = size - 2; i >= 0; i--)
			solution[i] = alpha[i] * solution[i + 1] + beta[i];
	};
	std::vector<double> getSolution()
	{
		return solution;
	};
};