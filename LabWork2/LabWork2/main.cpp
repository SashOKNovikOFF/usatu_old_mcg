#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <fstream>
#include <iomanip>

#include "HeatEquation.h"

void calculation()
{
	int numNode = 41;
	double dt = 1.0E-4;

	HeatEquation objHE(numNode, dt);

	double time = 400.0;
	double percent = 0.0;
	int step = 0;

	float fTimeStart = clock() / (float)CLOCKS_PER_SEC;
	while (objHE.getTime() < time) {

		objHE.calcOneStepTDMA();
		if (!objHE.getCheckResult())
		{
			std::cout << "FALSE" << std::endl;
			break;
		};
		if ((step % 10000) == 0) std::cout << "Step = " << percent << ", dt = " << objHE.getDt() << std::endl;
		percent = objHE.getTime() / time * 100.0;
		step++;
	};
	float fTimeFinish = clock() / (float)CLOCKS_PER_SEC;
	std::cout << "Time: " << fTimeFinish - fTimeStart << std::endl;

	objHE.printNumericalSolution("NUM-TDMA.txt");
}
std::vector<double> calculation(int numNode, double time, double dt, bool func)
{
	HeatEquation objHE(numNode, dt);

	while (objHE.getTime() < time)
		func ? objHE.calcOneStepTDMA() : objHE.calcOneStepEXPL();

	objHE.printNumericalSolution(std::to_string(numNode) + ".txt");
	return objHE.getSolution();
}
double normVector(std::vector<double> v1, std::vector<double> v2)
{
	std::vector<double> temp = v1;

	for (std::size_t node = 0; node < temp.size(); node++)
		temp[node] = std::abs(v1[node] - v2[2 * node]);

	return *std::max_element(temp.begin(), temp.end());
}
void printData(std::vector<double> data, const std::string outputFile) {

	std::ofstream myFile(outputFile);

	myFile.precision(12);
	myFile << std::scientific;

	for (std::size_t node = 0; node < data.size(); node++)
		myFile << data[node] << std::endl;
	myFile.close();
};

int main()
{
	int numNode = 51;
	double time = 400.0;
	double dt = 1.0E+3;
	bool func = true;
	int numberSteps = 10000;
	std::string outputFile = "Solution.txt";

	std::vector<double> sol1, sol2, diff;

	HeatEquation objEXAC(numNode, time / numberSteps);
	HeatEquation objTDMA(numNode, time / numberSteps);

	for (int step = 0; step < numberSteps; step++)
	{
		objEXAC.calcOneStepEXPL();
		objTDMA.calcOneStepTDMA();
	};
	
	sol1 = objEXAC.getSolution();
	sol2 = objTDMA.getSolution();
	diff.push_back(normVector(sol1, sol2));

	std::ofstream myFile(outputFile);

	myFile.precision(12);
	myFile << std::scientific;

	myFile << std::setw(12) << "  DX" << " ";
	myFile << std::setw(12) << "EXAC" << " ";
	myFile << std::setw(12) << "TDMA" << " ";
	myFile << std::setw(12) << "DIFF" << std::endl;

	for (std::size_t node = 0; node < sol1.size(); node++)
		myFile << objEXAC.getDx() * node << " " << sol1[node] << " " << sol2[node] << " " << std::abs(sol1[node] - sol2[node]) << std::endl;
	myFile.close();

	return 0;
};