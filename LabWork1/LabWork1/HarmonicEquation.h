#define _USE_MATH_DEFINES // ������������ �������������� ��������� � Visual Studio

#ifndef HARMONICEQUATION_H
#define HARMONICEQUATION_H

// ����������� ������������ �����
#include <vector>
#include <fstream>
#include <iomanip>
#include <cmath>

#include "TDM_Algorithm.h"

class HarmonicEquation                        //!< ��������� ���������
{
private:

	const double leftBorder = 0.;
	const double rightBorder = M_PI / 2.;
	
	const double leftValue = 0.;
	const double rightValue = 0.;

	const double C1 = 0.;
	const double C2 = M_PI / 2.;

	int numNodes;
	double dx;

	std::vector<double> solution;
public:
	
	HarmonicEquation::HarmonicEquation(int numNodes_) :
		numNodes(numNodes_), dx((rightBorder - leftBorder) / (numNodes - 1)), solution(numNodes, 0.0)
	{
		solution[0] = leftValue;
		solution[numNodes - 1] = rightValue;
	}
	std::vector<double> HarmonicEquation::getSolution()
	{
		TDM_Algorithm solver(numNodes);

		solver.setLeftBound(1.0, 0.0, leftValue);
		solver.setRightBound(0.0, 1.0, rightValue);

		for (int node = 1; node < numNodes - 1; node++)
			solver.setValues(node, 1.0, dx * dx - 2.0, 1.0, -dx * dx * dx * node);

		solver.solve();
		solution = solver.getSolution();

		return solution;
	}
	double HarmonicEquation::getAnalyticalValue(double x)
	{
		return -x + C1 * cos(x) + C2 * sin(x);
	}
	void HarmonicEquation::getResultFile(std::string fileName)
	{
		std::ofstream myFile(fileName);

		myFile.precision(12);
		myFile << std::scientific;

		myFile << std::setw(12) << "X"     << " ";
		myFile << std::setw(12)  << "NUMER" << " ";
		myFile << std::setw(12)  << "EXACT" << std::endl;

		for (int node = 0; node < numNodes; node++)
			myFile << node * dx << " " << solution[node] << " " << getAnalyticalValue(node * dx) << std::endl;
		myFile.close();
	}
	double getDx() { return dx; }
};

#endif // HARMONICEQUATION_H
