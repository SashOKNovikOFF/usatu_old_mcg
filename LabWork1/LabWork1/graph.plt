reset

set terminal wxt persist

set logscale xy 2
set xtics add (201)
set xtics add (25601)

set xrange [201:25601]
#set xrange [0:pi/2]
#set xrange [0:8]

set xlabel "Число узлов сетки N"
set ylabel "||U_{N}(X) - U_{N+1}(X)||"

plot "DiffResult_2.txt" using 1:2 with lines title "По норме C_2", \
     "DiffResult_2.txt" using 1:3 with lines title "По норме L_2"