#pragma once

#include <vector>
#include <cmath>
#include "FDM_Algorithm.h"

class ODE4;

class ODE4
{
protected:
    
    double leftBorder = 0.0;
    double rightBorder = 8.0;
    
    double leftValue = 0.0;
    double rightValue = 4.0;
    
    double leftDValue = 1.2;
    double rightDValue = 2.0;
    
    int numNodes;
    double dx;
    
    std::vector<double> solution;
    
public:

    ODE4(int numNodes_) :
        numNodes(numNodes_), dx((rightBorder - leftBorder) / (numNodes - 1)), solution (numNodes, 0.0)
    {
        
    }
    void solve()
    {
        FDM_Algorithm solver(numNodes);
        solver.setLeft_0_Values(1.0, 0.0, 0.0, 0.0);
        solver.setLeft_1_Values(1.0, -1.0, 0.0, 0.0, - dx / 2.0);
        solver.setRight_N1_Values(0.0, 0.0, 1.0, -1.0, - 2.0 * dx);
        solver.setRight_N_Values(0.0, 0.0, 1.0, 4.0);
        
		for (int node = 2; node <= numNodes - 2; node++)
			solver.setValues(node, 1.0, -4.0 + 3.0 * dx * dx, 6.0 - 6.0 * dx * dx, -4.0 + 3.0 * dx * dx, 1.0, pow(dx, 4.0) * (dx * node - cos(dx * node / 2.0)));//3 * dx * dx - 4.0, 6.0 - 6.0 * dx * dx, 3 * dx * dx - 4.0, 1.0, dx * dx * dx * dx * (dx * node - cos(dx / 2.0)));
        
        solver.solve();
        solution = solver.getSolution();
    }
    std::vector<double> getSolution() { return solution; }
	double getDx() { return dx; }
};