#define _USE_MATH_DEFINES // ������������ �������������� ��������� � Visual Studio

// ���������������� ������������ �����
#include "HarmonicEquation.h"
#include "ODE4.h"

// ����������� ������������ �����
#include <cmath>
#include <fstream>
#include <algorithm>
#include <iostream>
#include <iomanip>

double normVectorC(std::vector<double> v1, std::vector<double> v2)
{
	std::vector<double> temp(v1.size(), 0.0);

	for (std::size_t node = 0; node < temp.size() - 1; node++)
		temp[node] = std::abs(v1[node] - v2[2 * node]);

	return *std::max_element(temp.begin(), temp.end());
}
double normVectorL2(std::vector<double> v1, std::vector<double> v2)
{
	double sum = 0;
	for (std::size_t node = 0; node < v1.size() - 1; node++)
		sum += pow(v1[node] - v2[2 * node], 2.0);
	
	return sum = sqrt(sum / (v1.size() + 1));
}
void printData(std::vector<double> data, const std::string outputFile) {

	std::ofstream myFile(outputFile);

	myFile.precision(12);
	myFile << std::scientific;

	for (std::size_t node = 0; node < data.size(); node++)
		myFile << data[node] << std::endl;
	myFile.close();
}
void diff_1_calc()
{
	int numNode = 101;

	HarmonicEquation temp(numNode);

	std::vector<double> sol1, sol2, diffC, diffL2;

	sol1 = temp.getSolution();
	for (int i = 1; i < 20; i++)
	{
		HarmonicEquation obj((numNode - 1) * 2 + 1);

		sol2 = obj.getSolution();

		diffC.push_back(normVectorC(sol1, sol2));
		diffL2.push_back(normVectorL2(sol1, sol2));

		std::cout << "Diff_C (" << numNode << ", " << (numNode - 1) * 2 + 1 << "): " << diffC[diffC.size() - 1] << std::endl;
		std::cout << "Diff_L2 (" << numNode << ", " << (numNode - 1) * 2 + 1 << "): " << diffL2[diffL2.size() - 1] << std::endl;

		sol1 = sol2;
		numNode = (numNode - 1) * 2 + 1;
	};

	std::ofstream myFile("DiffResult.txt");

	myFile.precision(12);
	myFile << std::scientific;

	myFile << std::setw(12) << "NODES" << " ";
	myFile << std::setw(12) << "DIFF_C" << " ";
	myFile << std::setw(12) << "DIFF_L2" << std::endl;

	numNode = 101;
	for (std::size_t node = 0; node < diffC.size(); node++)
		myFile << (numNode - 1) * (int)pow(2, (node + 1)) + 1 << " " << diffC[node] << " " << diffL2[node] << std::endl;
	myFile.close();
}
double func(double x)
{
	//return x * (16 * pow(x, 4.0) - 3027.0 * x * x + 16024.0 * x + 960.0) / 1920.0;
	return 0.0000305626 * (1817.76 * pow(x, 3.0) - 83747.0 * x + 57796.7 * sin(1.73205 * x) + 47592.3 * cos(0.5 * x) + 147558.0 * cos(1.73205 * x) - 195150.0);
	//return 2.090262935*sin(1.732050808*x)+4.266112308*cos(1.732050808*x)+1.454545455*cos(.5000000000*x)+0.5555555556e-1*x*x*x-3.120441606*x-5.720657763;
}
void temp()
{
	int nodes = 12801;
	double dx = 8.0 / (nodes);

	ODE4 obj(nodes);

	obj.solve();

	std::vector<double> sol = obj.getSolution();

	std::ofstream myFile("ODE4_result.txt");

	myFile.precision(12);
	myFile << std::scientific;

	myFile << std::setw(12) << "NODES" << " ";
	myFile << std::setw(12) << "VALUE_NUM" << " ";
	myFile << std::setw(12) << "VALUE_EXA" << std::endl;

	for (std::size_t node = 0; node < sol.size(); node++)
		myFile << node * obj.getDx() << " " << sol[node] << " " << func(node * dx) << std::endl;
	myFile.close();
}
void diff_2_calc()
{
	int numNode = 101;

	ODE4 temp(numNode);

	std::vector<double> sol1, sol2, diffC, diffL2;

	temp.solve();
	sol1 = temp.getSolution();
	for (int i = 1; i < 15; i++)
	{
		ODE4 obj((numNode - 1) * 2 + 1);

		obj.solve();
		sol2 = obj.getSolution();

		diffC.push_back(normVectorC(sol1, sol2));
		diffL2.push_back(normVectorL2(sol1, sol2));

		std::cout << "Diff_C (" << numNode << ", " << (numNode - 1) * 2 + 1 << "): " << diffC[diffC.size() - 1] << std::endl;
		std::cout << "Diff_L2 (" << numNode << ", " << (numNode - 1) * 2 + 1 << "): " << diffL2[diffL2.size() - 1] << std::endl;

		sol1 = sol2;
		numNode = (numNode - 1) * 2 + 1;
	};

	std::ofstream myFile("DiffResult.txt");

	myFile.precision(12);
	myFile << std::scientific;

	myFile << std::setw(12) << "NODES" << " ";
	myFile << std::setw(12) << "DIFF_C" << " ";
	myFile << std::setw(12) << "DIFF_L2" << std::endl;

	numNode = 101;
	for (std::size_t node = 0; node < diffC.size(); node++)
		myFile << (numNode - 1) * (int)pow(2, (node + 1)) + 1 << " " << diffC[node] << " " << diffL2[node] << std::endl;
	myFile.close();
}

int main()
{
	HarmonicEquation obj(12801);

	obj.getSolution();
	obj.getResultFile("ODE2_result.txt");

	return 0;
};