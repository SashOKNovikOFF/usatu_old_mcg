#pragma once

#include <vector>

class FDM_Algorithm;

class FDM_Algorithm
{
protected:

	int N;
	
	std::vector< std::vector<double> > matrix;
	
	std::vector<double> alpha;
	std::vector<double> beta;
	std::vector<double> gamma;
	
	std::vector<double> solution;

public:

	FDM_Algorithm(int N_) : N(N_),
		matrix(6, std::vector<double>(N_ + 1, 0.0)),
		alpha(N_ + 1, 0.0),
		beta(N_, 0.0),
		gamma(N_ + 2, 0.0),
		solution(N_ + 1, 0.0) { };

	void setLeft_0_Values(double c0, double d0, double e0, double f0)
	{
		matrix[0][0] = 0.0;
		matrix[1][0] = 0.0;
		matrix[2][0] = c0;
		matrix[3][0] = -d0;
		matrix[4][0] = e0;
		matrix[5][0] = f0;
	};
	void setLeft_1_Values(double b1, double c1, double d1, double e1, double f1)
	{
		matrix[0][1] = 0.0;
		matrix[1][1] = -b1;
		matrix[2][1] = c1;
		matrix[3][1] = -d1;
		matrix[4][1] = e1;
		matrix[5][1] = f1;
	};
	void setRight_N1_Values(double aN1, double bN1, double cN1, double dN1, double fN1)
	{
		matrix[0][N - 1] = aN1;
		matrix[1][N - 1] = -bN1;
		matrix[2][N - 1] = cN1;
		matrix[3][N - 1] = -dN1;
		matrix[4][N - 1] = 0.0;
		matrix[5][N - 1] = fN1;
	};
	void setRight_N_Values(double aN, double bN, double cN, double fN)
	{
		matrix[0][N] = aN;
		matrix[1][N] = -bN;
		matrix[2][N] = cN;
		matrix[3][N] = 0.0;
		matrix[4][N] = 0.0;
		matrix[5][N] = fN;
	};
	void setValues(int node, double ai, double bi, double ci, double di, double ei, double fi)
	{
		matrix[0][node] = ai;
		matrix[1][node] = -bi;
		matrix[2][node] = ci;
		matrix[3][node] = -di;
		matrix[4][node] = ei;
		matrix[5][node] = fi;
	};
	void solve()
	{
		// Прямой ход прогонки
		
		alpha[1] = matrix[3][0] / matrix[2][0];
		 beta[1] = matrix[4][0] / matrix[2][0];
		gamma[1] = matrix[5][0] / matrix[2][0];
		
		double delta_1 = matrix[2][1] - alpha[1] * matrix[1][1];
		alpha[2] = (matrix[3][1] - beta[1] * matrix[1][1]) / delta_1;
		 beta[2] = matrix[4][1] / delta_1;
		gamma[2] = (matrix[5][1] + gamma[1] * matrix[1][1]) / delta_1;
		
		for (int i = 3; i <= (N - 1); i++)
		{
		    double delta_i1 = matrix[2][i - 1] - matrix[0][i - 1] * beta[i - 2] + alpha[i - 1] * (matrix[0][i - 1] * alpha[i - 2] - matrix[1][i - 1]);
		    alpha[i] = (matrix[3][i - 1] + beta[i - 1] * (matrix[0][i - 1] * alpha[i - 2] - matrix[1][i - 1])) / delta_i1;
		     beta[i] = matrix[4][i - 1] / delta_i1;
		    gamma[i] = (matrix[5][i - 1] - matrix[0][i - 1] * gamma[i - 2] - gamma[i - 1] * (matrix[0][i - 1] * alpha[i - 2] - matrix[1][i - 1])) / delta_i1;
		};
		
		double delta_N1 = matrix[2][N - 1] - matrix[0][N - 1] * beta[N - 2] + alpha[N - 1] * (matrix[0][N - 1] * alpha[N - 2] - matrix[1][N - 1]);
		alpha[N] = (matrix[3][N - 1] + beta[N - 1] * (matrix[0][N - 1] * alpha[N - 2] - matrix[1][N - 1])) / delta_N1;
		gamma[N] = (matrix[5][N - 1] - matrix[0][N - 1] * gamma[N - 2] - gamma[N - 1] * (matrix[0][N - 1] * alpha[N - 2] - matrix[1][N - 1])) / delta_N1;

		double delta_N = matrix[2][N] - matrix[0][N] * beta[N - 1] + alpha[N] * (matrix[0][N] * alpha[N - 1] - matrix[1][N]);
		gamma[N + 1] = (matrix[5][N] - matrix[0][N] * gamma[N - 1] - gamma[N] * (matrix[0][N] * alpha[N - 1] - matrix[1][N])) / delta_N;

		// Обратный ход прогонки

		solution[N] = gamma[N + 1];
		solution[N - 1] = alpha[N] * solution[N] + gamma[N];
		for (int i = N - 2; i >= 0; i--)
			solution[i] = alpha[i + 1] * solution[i + 1] - beta[i + 1] * solution[i + 2] + gamma[i + 1];
	};
	std::vector<double> getSolution()
	{
		return solution;
	};
};